=== Wordpress Facebook Pixel  ===

Contributors: Cristian Tala S.



Tags: facebook, pixel

Requires at least: 3, 3.3+

Tested up to: 3.9

Stable tag: 0.1

== Description ==

This plugin enables the facebook code needed for the remarketing campaigns.
